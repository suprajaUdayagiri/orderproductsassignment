from curses.ascii import US
from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from .models import Product
from .models import User
from .models import Order

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['pid', 'name', 'price', 'qtyInStck']

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['userId', 'name', 'password']

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['orderId', 'productId', 'userId', 'qty']