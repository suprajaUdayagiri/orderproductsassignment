from asyncio.windows_events import NULL
from curses.ascii import NUL
import imp
from django.http import JsonResponse
from .models import Order, Product, User
from .serializers import OrderProductsSerializer
from .serializers import OrderSerializer
from .serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status

def authUser(request, uname, pwd):
    try:
        userExists = User.objects.get(name=uname,passwoord = pwd)
        if (userExists):
            return(True)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    return(False)

def productsList(request, format=None):
    products = Product.objects.all()
    productSerializer = OrderProductsSerializer(products, many=True)
    return JsonResponse({'product' : productSerializer.data})

def OrderListbyUserId(request, uId):
    try:
        orders = Order.objects.get(userId=uId)
    except Order.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    orderSerializer = OrderSerializer(orders)
    return Response(OrderSerializer.data)
    

def OrderListbyProductId(request, pId):
    try:
        orders = Order.objects.get(userId=pId)
    except Order.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    orderSerializer = OrderSerializer(orders)
    return Response(OrderSerializer.data)

def placeOrder(request, pid_, uid, qty_, format=None):
    order = Order(productId=pid_, userId = uid, qty=qty_)
    product = Product.objects.get(pid=pid_).qty 
    if (product.qty >= qty_ and User.objects.get(userId=uid) != NULL):
        order.save()
        product.qty -= qty_
        product.save()
        return(True)
    return(False)
