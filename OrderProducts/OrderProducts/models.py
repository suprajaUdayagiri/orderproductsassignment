from django.db import models

class Product(models.Model):
    pid = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    price = models.FloatField(default="100")
    qtyInStck = models.IntegerField(default="10")

    def __str__(self) -> str:
        return "Name: " + self.name + ", id : " + str (self.id) + ", price: " + str(self.price) + ", qtyInStck: " + str(self.qtyInStck)

class User(models.Model):
    userId = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    
    def __str__(self) -> str:
        return "userId: " + str(self.userId) + ", name: " + self.name + ", password: " + self.password

class Order(models.Model):
    orderId = models.IntegerField(primary_key=True)
    productId = models.ForeignKey(Product, on_delete=models.CASCADE)
    userId = models.ForeignKey(User, on_delete=models.CASCADE)
    qty = models.IntegerField()

    def __str__(self) -> str:
        return "orderId: " + str(self.orderId) + ", productId: " + str(self.productId.pid) + ", userId: " + str(self.userId.userId) + ", quantity: " + str(self.qty)