# Generated by Django 4.1.2 on 2022-10-07 17:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('OrderProducts', '0002_rename_order_orders_rename_user_users'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Orders',
            new_name='Order',
        ),
        migrations.RenameModel(
            old_name='Products',
            new_name='Product',
        ),
        migrations.RenameModel(
            old_name='Users',
            new_name='User',
        ),
    ]
