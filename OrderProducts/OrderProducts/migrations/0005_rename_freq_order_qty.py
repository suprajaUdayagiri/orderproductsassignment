# Generated by Django 4.1.2 on 2022-10-07 18:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('OrderProducts', '0004_rename_id_product_pid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='freq',
            new_name='qty',
        ),
    ]
